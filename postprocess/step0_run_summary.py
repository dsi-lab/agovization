import sys
import ast
import numpy as np
import tabloo
import pickle
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting
import pandas as pd
import os

# example command: 
# python   postprocess/step0_run_summary.py  /Volumes/data/Gilgamesh/kroppian/agovization_results/*

directories = sys.argv[1:]

output_dir_seg = os.path.realpath(__file__).split("/")

output_dir = "/".join(output_dir_seg[0:-1])

IRR_COL = 1

raw_master_table = {'year':[], 'yield':[], 'leaching':[], 'irr_total':[], 'irr_app_count':[], 'front':[], 'scheds':[]}

raw_yrly_sum_tab = {
    "year" : [],
    "plant date" : [],
    "irr period start" : [],
    "irr period end": [],
    "nit period start" : [],
    "nit period end" : [],
    "max yield (kg/ha)": [],
    "mean I.A.C." : [],
    "median I.A.C." : [],
    "mean irr total (mm)" : [],
    "median irr total (mm)" : [],
    "mean leaching (kg/ha)" : [],
    "median leaching (kg/ha)" : [], 
    "min leaching (kg/ha)": [], 
    "max leaching (kg/ha)": []}



for directory in directories:

    # Get the genome struct info 
    genome_struct_path = "%s/genome_structure.py" % directory

    with open(genome_struct_path, 'r') as f: genome_struct = ast.literal_eval(f.read())

    # Get the pickled run record
    run_record_path = "%s/run_sim_record.pkl" % directory
    infile = open(run_record_path, 'rb')
    run_record = pickle.load(infile)

    # Year 
    path_segments = directory.split("/")
    year = int(path_segments[-1][5:9])

    print("Processing year %d for folder %s" % (year, directory))

    # Plant date 
    plant_date = 135

    if year % 4 == 0:
        plant_date += 1

    # Irr date info
    periods = genome_struct['date_ranges']
    irr_period = periods[0]
    irr_start = irr_period[0] - (year * 1e3)
    irr_end = irr_period[1] - (year * 1e3)

    # Nit date info
    nit_period = periods[1]
    nit_start = nit_period[0] - (year * 1e3)
    nit_end = nit_period[1] - (year * 1e3)

    # Yield information 
    max_yield = max(run_record['yield'])

    # Irrigation application counts and totals
    run_scheds = run_record['scheds'].tolist()
    #                               Only select the irr applications
    #                               V
    irr_app_count = [np.shape(sched[sched[:,IRR_COL] != 0])[0] for sched in run_scheds]
    #                          Only select the irr applications
    #                          V
    irr_app_total = [sum(sched[sched[:,IRR_COL] != 0][:,IRR_COL]) for sched in run_scheds]

    run_record['irr_app_count'] = irr_app_count
    run_record['irr_total'] = irr_app_total

    # Non-dominated sort
    f1 = -run_record['yield'] # Made this negative because we to maximize yield
    f2 = run_record['leaching']
    f3 = run_record['irr_total']
    F = np.column_stack((f1,f2,f3))

    record_count = np.shape(run_record)[0]

    opt_fronts = NonDominatedSorting().do(F)

    #non_dom = [True if i in opt_front_i else False for i in range(record_count)]
    #run_record['non_dom'] = non_dom
     
    sorted_fronts = np.array([-1 for i in range(record_count)])

    for (f, front) in enumerate(opt_fronts):
        sorted_fronts[front] = f

    run_record['front'] = sorted_fronts.tolist()

    # make these only the optimal solutions
    irr_count_mean = np.mean(run_record[run_record['front'] == 0]['irr_app_count'])
    irr_count_median = np.median(run_record[run_record['front'] == 0]['irr_app_count'])

    irr_total_mean = np.mean(run_record[run_record['front'] == 0]['irr_total'])
    irr_total_median = np.median(run_record[run_record['front'] == 0]['irr_total'])

    # Calculate leaching statistics
    leaching_mean = np.mean(run_record[run_record['front'] == 0]['leaching'])
    leaching_median = np.median(run_record[run_record['front'] == 0]['leaching'])
    leaching_min = min(run_record[run_record['front'] == 0]['leaching'])
    leaching_max = max(run_record[run_record['front'] == 0]['leaching'])

    raw_yrly_sum_tab["year"] .append(year)
    raw_yrly_sum_tab["plant date"] .append(plant_date)
    raw_yrly_sum_tab["irr period start"] .append(irr_start)
    raw_yrly_sum_tab["irr period end"] .append(irr_end)
    raw_yrly_sum_tab["nit period start"] .append(nit_start)
    raw_yrly_sum_tab["nit period end"] .append(nit_end)
    raw_yrly_sum_tab["max yield (kg/ha)"] .append(max_yield)
    raw_yrly_sum_tab["mean I.A.C."] .append(irr_count_mean)
    raw_yrly_sum_tab["median I.A.C."] .append(irr_count_median)
    raw_yrly_sum_tab["mean irr total (mm)"] .append(irr_total_mean)
    raw_yrly_sum_tab["median irr total (mm)"] .append(irr_total_median)
    raw_yrly_sum_tab["mean leaching (kg/ha)"] .append(leaching_mean)
    raw_yrly_sum_tab["median leaching (kg/ha)"] .append(leaching_median)
    raw_yrly_sum_tab["min leaching (kg/ha)"] .append(leaching_min)
    raw_yrly_sum_tab["max leaching (kg/ha)"] .append(leaching_max)

    # Build out the master table
    raw_master_table['year'] = raw_master_table['year'] + ([year] * record_count)

    for key in raw_master_table.keys():
        if key == 'year':
            continue
        raw_master_table[key] = raw_master_table[key] + run_record[key].tolist()


master_run_record = pd.DataFrame(raw_master_table)
yrly_sum_tab = pd.DataFrame(raw_yrly_sum_tab)

output = open('%s/master_run_record.pkl' % output_dir, 'wb')
pickle.dump(master_run_record, output)

output = open('%s/yearly_summary.pkl' % output_dir, 'wb')
pickle.dump(yrly_sum_tab, output)

# Creating XLSX output
print("Saving to excel...")

with pd.ExcelWriter('run_results.xlsx') as writer:  
    master_run_record.to_excel(writer, sheet_name='RunRecord')
    yrly_sum_tab.to_excel(writer, sheet_name='YearlySummary')


print("Done")


